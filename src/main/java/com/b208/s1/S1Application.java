package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
//It will tell springboot that this application will function as an endpoint handling web requests.
public class S1Application {
	//Annotations in Java Springboot marks classes for their intended functionalities.
	//Sptingboot upon startup scans for classes and assign based on their annotation.
	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);
	}

	//@GetMapping will map a route or an endpoint to access or run our method.
	//When http://localhost:8080/hello is accessed from a client, we will be able to run the hello() method.
	@GetMapping("/hello")
	public String hello() {
		return "Hi from our Java Springboot App!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "John") String nameParameter){
		//Pass data through the url using our string query
		//http://localhost:8080/hi?name=Joe
		//WE retrieve the value of the field "name" from out URL
		//defaultValue is the fallback value when the query string ot request param is empty.
		//System.out.println(nameParameter);
		return "Hi! My name is " + nameParameter;
	}

	@GetMapping("/myFavoriteFood")
	public String myFavoriteFood(@RequestParam(value = "food", defaultValue = "Sisig") String foodParameter){
		//http://localhost:8080/myFavoriteFood?food=pizza
		//can store multiple req params
		return "Hi! My favorite food is " + foodParameter;

	}

	//2 ways of passing data through the URL by using Java Springboot.
	//Query String using Request Params - Directly passing data into the URL and getting the data
	//Path Variable is much more similar to ExpressJS's req.params

	@GetMapping("/greeting/{name}")
	public String greeting(@PathVariable("name") String nameParams){
		//@PathVariable() allows us to extract data directly from the url.
		//usually used for dynamic URL
		//name name should be same , getmap & pathvar
		return "Hello " + nameParams;
	}

	ArrayList<String> enrollees = new ArrayList<>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user") String enrollParams){

		enrollees.add(enrollParams);
		return "Welcome, " + enrollParams + "!";

	}

	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees() {
		for (int i = 0; i < enrollees.size(); i++) {

			return enrollees;

		}

		return null;
	}

	@GetMapping("/courses/{id}")
	public String getCourse(@PathVariable("id") String idParams) {

		switch (idParams) {

			case "java101":
				return "Name: Java 101, Schedule MWF 8:00 AM - 11:00 AM, Price: P3000";

			case "sql101":
				return "Name: SQL 101, Schedule TTHS 8:00 AM - 11:00 AM, Price: P3000";

			case "jsoop101":
				return "Name: JS OOP 101, Schedule MWF 1:00 PM - 5:00 PM, Price: P5000";

			default:
				return "Course cannot be found.";
		}
	}






	}
